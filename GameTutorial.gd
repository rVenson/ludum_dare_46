extends Game

onready var Chatbox : Chatbox = get_node("/root/Scene/Chatbox/")

var tutorial_index = 0

func _ready():
	pause_game(false)
	randomize()
	generate_game_timer()
	$ChatTimer.start()

func generate_spawn_timer():
	spawn_timer = Timer.new()
	spawn_timer.autostart = false
	spawn_timer.one_shot = true
	spawn_timer.wait_time = 100
	spawn_timer.process_mode = Timer.TIMER_PROCESS_PHYSICS
	spawn_timer.connect("timeout", self, "on_SpawnTimer_timeout")
	add_child(spawn_timer)
	
func generate_game_timer():
	game_timer = Timer.new()
	game_timer.autostart = false
	game_timer.one_shot = true
	game_timer.wait_time = game_time
	game_timer.process_mode = Timer.TIMER_PROCESS_PHYSICS
	game_timer.connect("timeout", self, "on_GameTimer_timeout")
	add_child(game_timer)

func _on_ChatTimer_timeout():
	Chatbox.print_message("Oh! Sorry for delay. I was helping the a stupid hard disk...")
	Chatbox.print_message("I think you are the new piece of software... welcome!")
	Chatbox.print_message("Where I am? I'm the motherboard you folish!")
	Chatbox.print_message("We need start now. You can see this package that arrives? You need to analyze this before crashing things out there.")
	Chatbox.print_message("Put it on Analyzer to learn about it")

func tutorial01():
	var spawned_packet : Packet = input_treadmill.spawn_packet()
	spawned_packet.port = 80
	spawned_packet.expiration = 200
	spawned_packet.workload = 12
	UI.print_feedback("A packet arrives")
	Sound.generate_fx_sound("beep01")

func _on_Chatbox_end_of_pool():
	if tutorial_index == 0:
		tutorial01()
		tutorial_index += 1
	if tutorial_index == 2:
		get_tree().change_scene("res://Scene.tscn")

func _on_AnalyzerTable_push_packet():
	Chatbox.print_message("Now you need to put this packet in the right port")
	Chatbox.print_message("We have only one application running on the port 80 now, don't be a silly robot...")


func _on_TreadmillResponse_response_ready():
	Chatbox.print_message("When a packet go through a application, they consume system resources. You need to observe this. DON'T CRASH THE SYSTEM!")
	Chatbox.print_message("Your response is ready for be submited to the output. Do this before the packet expires and the connection is lost FOREVER")

func _on_TreadmillWithResponseMachine_response_sent():
	tutorial_index = 2
	Chatbox.print_message("You really are a robot? You so nice!")
	Chatbox.print_message("No, I was not talking to you NEWBIE. There is another machine learning robot here that is awesome and...")
	Chatbox.print_message("Did you finish your job? Okay, I'm going to put you into production. But FOR ALAN TURING don't CRASH EVERYTHING on the first day")
	Chatbox.print_message("Remember to keep the workload low and don't miss the packet ports.")
	Chatbox.print_message("In case of malicious or expired package, you can always use the sink hole to discard them.")
	Chatbox.print_message("FORK APPLICATION in 3, 2, 1...")
	Chatbox.print_message("...")
