# LUDUM DARE 46
* Theme: Keep It Alive

## Brainstorm

* Any thing/life that need to be protected
* Die without protection
* Time/Wave protection
* Green time ball
* Fire, collect resources to keep the fire alive
* Keep Alive, you need to distribuite the packages of the connections

## Brainstorm 2

The stressful life of an web server

* You need to deliver packages on right places
* You receive packages on your home (server) and need respond
* Packages have some atributes:
  * From
  * To
  * Message
* You need to deny malicious packages
* You need to deny DOS atacks
* Kill malicous packages? How identify?
* Waves of Packages
* You could deny ips malicious (send the to sink hole)
* You need to route Packages
* Web server have an indicator of usage
* Objective: process the most number of packages without broken the server

## Idea

* Objective: process the most number of packages without broken the server
* Packages arrive on a treadmill
* The player needs to pick up the packages and put on Analyzer Table
* The Analyzer shows the port, expiration time and processing workload
* The player needs to put the package and the right treadmill
* The package will be processed
* The responses of applications arrive in an extra treadmill
* You need put these responses on Exit treadmill
* There is a await zone where you can put packages
* Packages can have different colors
* The server has a workload meter
* If the workload is greater than webserver support, they will shutdown
* Upon shutdown, all the packages will go to the response treadmill
* You need to pull a lever to restart the server
* But only if the response treadmill is free
* You have two minutes per game
* In the end, you receive a grade by the number of packages processed

### Stages

* First: App port and process workload
* Second: App port, process workload and expiration (expired packages could be discarded)
* Third: App port, process workload, expiration date and from (deny packages from a IP list)
