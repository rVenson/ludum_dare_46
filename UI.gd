extends Control

class_name UI

onready var Game : Game = get_node("/root/Scene/Loop/Game")

func _process(delta):
	if !Game.end_game:
		if(Input.is_action_just_pressed("ui_cancel")):
			$PauseMenu.visible = !$PauseMenu.visible
			Game.pause_game($PauseMenu.visible)
		if(Input.is_action_just_pressed("ui_page_down")):
			Game.game_over()
		if(Input.is_action_just_pressed("ui_page_up")):
			Game.win()

func _physics_process(delta):
	$Crono.text = "Time Left\n%3.1f seconds" % Game.game_timer.time_left
	
	$Control/WorkloadProgressBar.value = Game.workload
	$Control/Popularity.text = "Alive Connections: %.0f" % Game.popularity

	if Game.analyzer != null:
		$Analyzer.text = "Type: " + str(Game.analyzer.type)
		$Analyzer.text += "\nPort: " + str(Game.analyzer.port)
		$Analyzer.text += "\nWorkload: +%.2f %%" % Game.analyzer.workload
		$Analyzer.text += "\nExpiration: %.0f seconds" % Game.analyzer.expiration
		$Analyzer.text += "\nCode: " + str(Game.analyzer.code)
	else:
		$Analyzer.text = "Put a packet on Analyzer to see details..."

func print_feedback(msg):
	var feedback = preload("res://FeedbackControl.tscn").instance()
	feedback.get_child(0).text = msg
	add_child(feedback)

func game_over():
	$GameOverMenu.visible = true
	Game.Animator.play("game_over")

func win(properties):
	$WinMenu.load_properties(properties)
	Game.Animator.play("win")
