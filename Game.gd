extends Node

class_name Game

var port_list = [80, 8080, 443]

var processing_packets = Array()
var analyzed_packets = 0
var workload = 0
var process_speed = 1
var sucessful_responses = 0
var analyzer = null
var end_game : bool = false
var game_time = 180

var popularity = 100
var discarded = 0

var spawn_timer : Timer = null
var game_timer : Timer = null

onready var input_treadmill = get_node("/root/Scene/Loop/Map/TreadmillInput")
onready var response_treadmill = get_node("/root/Scene/Loop/Map/TreadmillResponse")
onready var output_treadmill = get_node("/root/Scene/Loop/Map/TreadmillOutput")
onready var UI = get_node("/root/Scene/UI")
onready var Sound : SoundManager = get_node("/root/SoundManager")
onready var Animator : AnimationPlayer = get_node("/root/Scene/AnimationPlayer")

func _ready():
	pause_game(false)
	randomize()
	generate_spawn_timer()
	generate_game_timer()

func generate_spawn_timer():
	spawn_timer = Timer.new()
	spawn_timer.autostart = false
	spawn_timer.one_shot = true
	spawn_timer.wait_time = 1
	spawn_timer.process_mode = Timer.TIMER_PROCESS_PHYSICS
	spawn_timer.connect("timeout", self, "on_SpawnTimer_timeout")
	add_child(spawn_timer)
	spawn_timer.start()
	
func generate_game_timer():
	game_timer = Timer.new()
	game_timer.autostart = false
	game_timer.one_shot = true
	game_timer.wait_time = game_time
	game_timer.process_mode = Timer.TIMER_PROCESS_PHYSICS
	game_timer.connect("timeout", self, "on_GameTimer_timeout")
	add_child(game_timer)
	game_timer.start()

func remove_popularity(damage):
	discarded += 1
	popularity -= damage
	Sound.generate_fx_sound("damage")
	if popularity <= 0:
		game_over()

func add_analyzed_packets(count = 1):
	analyzed_packets += count

func add_response(packet):
	if packet.expiration > 0:
		sucessful_responses += 1
		UI.print_feedback("A sucessful response was sent")
	packet.queue_free()

func set_analyzer(packet = null):
	analyzer = packet

func process_packet(packet : Node):
	add_analyzed_packets()
	processing_packets.append(packet)
	UI.print_feedback("A packet is being analyzed. Workload +%.2f%%" % packet.workload)

func _physics_process(delta):
	workload = 0
	for packet in processing_packets:
		workload += packet.loaded_workload
		
		if packet.ready_for_process:
			packet.loaded_workload -= process_speed * delta
			
			if packet.loaded_workload <= 0:
				var response = response_treadmill.spawn_packet()
				response.type = response.TYPE.response
				processing_packets.erase(packet)
				UI.print_feedback("One packet response ready")
				Sound.generate_fx_sound("success01")
	
	if workload > 100:
		game_over()

func randomize_packet(packet : Packet):
	packet.port = port_list[rand_range(0, port_list.size())]
	packet.workload = rand_range(30, 60)
	packet.expiration = rand_range(40, 80)
	packet.code = 0

func on_SpawnTimer_timeout():
	var spawned_packet : Packet = input_treadmill.spawn_packet()
	randomize_packet(spawned_packet)
	spawn_timer.wait_time = rand_range(7, 12)
	spawn_timer.start()
	
	UI.print_feedback("A packet arrives")
	Sound.generate_fx_sound("beep01")

func on_GameTimer_timeout():
	win()

func pause_game(value):
	var loop : Spatial = get_node("/root/Scene/Loop")
	loop.get_tree().paused = value

func game_over():
	if !end_game:
		end_game = true
		pause_game(true)
		UI.game_over()

func win():
	if !end_game:
		end_game = true
		pause_game(true)
		UI.win({
			"Sucessful responses": sucessful_responses,
			"Processed packets": analyzed_packets,
			"Connections saved": popularity,
			"Wrongly discarded packets": discarded,
		})
