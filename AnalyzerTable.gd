extends StaticBody

var packet = null
onready var Game = get_node("/root/Scene/Loop/Game")

signal push_packet

func pop_packet():
	var packet = self.packet
	self.packet = null
	$Position3D.remove_child(packet)
	Game.set_analyzer()
	$AnalyzerRay.visible = false
	return packet

func push_packet(packet):
	if self.packet == null:
		$Position3D.add_child(packet)
		self.packet = packet
		Game.set_analyzer(packet)
		$AnalyzerRay.visible = true
		emit_signal("push_packet")
		return true
	else:
		return false
