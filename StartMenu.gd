extends Control

func _on_ButtonStart_pressed():
	get_tree().change_scene("res://LoadScreen.tscn")

func _on_ButtonExit_pressed():
	get_tree().quit()

func _on_ButtonStartWithoutTutorial_pressed():
	get_tree().change_scene("res://Scene.tscn")
