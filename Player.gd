extends KinematicBody

var speed : float = 5
var movement : Vector3
var rotate_angle_y : float = 0
onready var Game = get_node("/root/Scene/Loop/Game")

func _process(delta):
	if(Input.is_action_pressed("up")):
		movement.z -= 1
		rotate_angle_y = 180
	if(Input.is_action_pressed("down")):
		movement.z += 1
		rotate_angle_y = 0
	if(Input.is_action_pressed("left")):
		movement.x -= 1
		rotate_angle_y = -90
	if(Input.is_action_pressed("right")):
		movement.x += 1
		rotate_angle_y = 90
		
	if(Input.is_action_just_pressed("action")):
		do_action()

func _physics_process(delta):
	$robot.rotation_degrees = Vector3(0, rotate_angle_y, 0)
	move_and_collide(movement * speed * delta)
	movement = Vector3()
	
func _get_actual_item():
	if $ItemPosition.get_child_count() > 0:
		return $ItemPosition.get_child(0)
	else:
		return null
	
func do_action():
	var overlapping_bodies : Array = $ContactArea.get_overlapping_bodies()
	var actual_item = _get_actual_item()
	
	if actual_item:
		for body in overlapping_bodies:
			if body.is_in_group("table"):
				$ItemPosition.remove_child(actual_item)
				var result = body.push_packet(actual_item)
				Game.Sound.generate_fx_sound("put_in")
				if !result:
					$ItemPosition.add_child(actual_item)
					actual_item.transform.origin = Vector3()
				break
	else:
		for body in overlapping_bodies:
			if body.is_in_group("table"):
				var packet = body.pop_packet()
				Game.Sound.generate_fx_sound("pick_up")
				if packet:
					$ItemPosition.add_child(packet)
					packet.transform.origin = Vector3()
				break
