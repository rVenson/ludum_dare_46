extends Node

const fx_list = {
	"pick_up" : preload("res://Sounds/fx/pick_up01.ogg"),
	"put_in" : preload("res://Sounds/fx/pick_up02.ogg"),
	"delete" : preload("res://Sounds/fx/wash01.ogg"),
	"beep01" : preload("res://Sounds/fx/beep01.ogg"),
	"success01" : preload("res://Sounds/fx/success01.ogg"),
	"deny01" : preload("res://Sounds/fx/deny01.ogg"),
	"damage" : preload("res://Sounds/fx/damage.ogg")
}

func generate_fx_sound(sound_name):
	var audio_player : AudioStreamPlayer = AudioStreamPlayer.new()
	add_child(audio_player)
	audio_player.stream = fx_list[sound_name]
	audio_player.play()
	audio_player.pitch_scale = rand_range(0.9, 1.1)
	audio_player.connect("finished", self,"on_audio_finished", [audio_player])

func on_audio_finished(audio_player):
	audio_player.queue_free()
	
