extends "res://TreadmillWithMachine.gd"

signal response_sent

func _process_packet(packet):
	pop_packet()
	Game.add_response(packet)

func push_packet(packet, force_input = false):
	if packet.type == packet.TYPE.response:
		if input || force_input:
			$PacketSpawner.add_child(packet)
			packet_list.append(packet)
			emit_signal("response_sent")
			return true
		else:
			return false
	else:
		return false
