extends "res://InGameMenu.gd"

onready var property_item_object = preload("res://PropertyItem.tscn")
onready var Chatbox : Chatbox = self.get_node("Chatbox")

func load_properties(properties):
	for key in properties:
		var property_item = property_item_object.instance()
		property_item.get_child(0).text = "%s: " % key
		property_item.get_child(1).text = str(properties[key])
		$VBoxContainer/Main/PropertiesBox.add_child(property_item)

func _on_Timer_timeout():
	Chatbox.print_message("Sorry! We know that this victory screen is so shameful!")
	Chatbox.print_message("The foolish programmer thought he could end it in time with 20 minutes left to Ludum Dare's end.")
	Chatbox.print_message("But look on the bright side, you were able to finish the game.")
	Chatbox.print_message("If you are participating in the ludum dare, please remember that this poor programmer worked until the last minute to deliver this game. Make your review for him.")
	Chatbox.print_message("See you at the next ludum dare =)")
