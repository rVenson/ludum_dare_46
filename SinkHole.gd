extends StaticBody

onready var Game : Game = get_node("/root/Scene/Loop/Game")

func pop_packet():
	print("You cannot push packets from a sink =(")
	return null

func push_packet(packet):
	
	if packet.code == 0:
		Game.remove_popularity(10)
	
	packet.queue_free()
	Game.Sound.generate_fx_sound("delete")
	$Particles.restart()
	return true
