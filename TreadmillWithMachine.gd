extends "res://Treadmill.gd"

var analyze_packet_list = []
export var analyze_speed = 10
export var port = 80

func _physics_process(delta):
	for packet in analyze_packet_list:
		if packet.port == self.port && packet.expiration > 0:
			packet.loaded_workload += delta * analyze_speed
			if packet.loaded_workload >= packet.workload:
				analyze_packet_list.pop_back()
				if packet != null:
					pop_packet()
					packet.ready_for_process = true
		else:
			Game.UI.print_feedback("A packet expired")
			Game.remove_popularity(10)
			analyze_packet_list.pop_back()
			pop_packet()

func _on_Area_body_entered(body):
	if body.is_in_group("packet"):
		_process_packet(body)

func _process_packet(packet):
	if packet.port == self.port:
		if packet.expiration > 0:
			analyze_packet_list.append(packet)
			Game.process_packet(packet)
		else:
			pop_packet()
			Game.UI.print_feedback("A packet has been routed already expired")
	else:
		pop_packet()
		Game.UI.print_feedback("A packet was wrongly routed")
		Game.Sound.generate_fx_sound("deny01")
