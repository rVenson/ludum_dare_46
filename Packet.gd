extends StaticBody

class_name Packet

enum TYPE { request, response}

var type = TYPE.request
var port = 80
var workload = 10
var expiration = 60
var code = 0
var loaded_workload = 0
var ready_for_process = false

func _physics_process(delta):
	expiration -= delta
	$Viewport/Label.text = str("%.0f" % expiration)
	
	if expiration < 0:
		$Viewport/Label.text = ""
