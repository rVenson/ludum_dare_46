extends Control
class_name Chatbox

var message_pool = Array()
signal end_of_pool

export var speed = 1
var isPlayAnimation = false

func _process(delta):
	if(Input.is_action_just_pressed("enter")):
		if isPlayAnimation:
			$Box/Label.percent_visible = 100
		else:
			visible = false

func _physics_process(delta):
	if !visible:
		if message_pool.size() > 0:
			$Box/Label.text = message_pool.pop_front()
			$Box/Label.percent_visible = 0
			isPlayAnimation = true
			visible = true
			if message_pool.size() == 0:
				emit_signal("end_of_pool")
	
	if isPlayAnimation:
		$Box/Label.percent_visible += delta * speed
		
	if $Box/Label.percent_visible >= 1:
		isPlayAnimation = false
		
func print_message(message):
	message_pool.append(message)
