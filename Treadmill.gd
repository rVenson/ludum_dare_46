extends StaticBody

onready var Game = get_node("/root/Scene/Loop/Game")
var packet_list = Array()
export var treadmill_speed = 2
export var input : bool = false
export var output : bool = true
export var treadmill_name = "Treadmill"

signal response_ready

func _ready():
	$Viewport/Label.text = treadmill_name

func spawn_packet():
	var packet_load = preload("res://Packet.tscn")
	var packet = packet_load.instance()
	push_packet(packet, true)
	emit_signal("response_ready")
	return packet

func pop_packet(index = 0):
	if output:
		if packet_list.size() > 0:
			var packet = packet_list[index]
			packet_list.remove(index)
			$PacketSpawner.remove_child(packet)
			return packet

func push_packet(packet, force_input = false):
	if input || force_input:
		$PacketSpawner.add_child(packet)
		packet_list.append(packet)

func _physics_process(delta):
	var z_position_index = 6
	for packet in packet_list:
		if packet.transform.origin.z <= z_position_index:
			packet.transform.origin.z += treadmill_speed * delta
		z_position_index -= 1

func _on_Area_body_entered(body):
	if body.is_in_group("packet"):
		pass
